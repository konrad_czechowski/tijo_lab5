package pl.edu.pwsztar.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Movie with this ID does not exist")
public class MovieNotFound extends RuntimeException {
}
